<?php include "template/header.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
		
        <div class="card-body p-0">
          <form method="post">
			<?php
				if (isset($_SESSION['id_user'])){
				$datkode=$_SESSION['id_user'];
				$query="SELECT * FROM tabel_user WHERE id_user='$datkode'";
				$query_exe=mysqli_query($conn, $query);
				$row=mysqli_fetch_array($query_exe);
				
				if(isset($_POST['submit'])){
				$nama_lengkap=$_POST['nama_lengkap'];
				$query="UPDATE tabel_user
				SET
				nama_lengkap='$nama_lengkap'
				WHERE
				id_user='$datkode'";
				$query_exe=mysqli_query($conn, $query);
				echo '<script>window.location.replace("'.base_url('admin/daftar_user.php').'")</script>';			 								
			}
		?>
		
		<div class="box box-primary">
		<div class="box-body">
		
		<div class="form-group">
		<label> Username </label> 
		<input disabled type="text" name="username" value="<?php print $row['username'];?>" 
		class="form-control" placeholder="Username" required>
        </div><br> 
		
		<div class="form-group">
		<label> Nama Lengkap </label> 
		<input type="text" name="nama_lengkap" value="<?php print $row['nama_lengkap'];?>"
		class="form-control" placeholder="Nama Lengkap" required>
        </div><br> 

		<div class="form-group">
		<label> Role </label> 
		<input disabled type="text" name="role" value="<?php print $row['role'];?>"  
		class="form-control" placeholder="role" required>
        </div><br> 
		
		<div class="form-group">
		<label> Status </label> 
		<input disabled type="text" name="status" value="<?php print $row['status'];?>" 
		class="form-control" placeholder="status" required>
        </div><br> 
	
		<input name="submit" type="submit" class="btn btn-primary pull-right" title="Edit Profile" value="Edit Profile">
		</div>
		</div>

		<?php } ?>
		</form>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include "template/footer.php"; ?>