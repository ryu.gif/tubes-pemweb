<?php include "template/header.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Lantai</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
    <div class="row">
      <div class="col-md-6">
      <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Tambah Lantai</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form action='' method='post'>
            <div class="box-body">
              <div class="form-group">
                <label for="nama_lantai">Nama Lantai</label>
                <input type="text" class="form-control" id="nama_lantai" name="nama_lantai" placeholder="Masukkan Nama Lantai" required>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <input type="submit" class="btn btn-success pull-right" name="submit" value="Simpan">
            </div>
          </form>
        </div>
      </div>
    </div>
    </section>

  <!-- /.content-wrapper -->
  </div>
  </div>

  <?php
  if(isset($_POST['submit'])){
		$nama_lantai=$_POST['nama_lantai'];
		$id_hotel=$_GET['id_hotel'];
		mysqli_query($conn, "INSERT INTO tabel_lantai (`nama_lantai`, `id_hotel`)
										 VALUES
										('$nama_lantai', '$id_hotel')");
		echo '<script>window.location.replace("'.base_url('petugas/beranda.php?id_hotel=').$_GET['id_hotel'].'")</script>';
		}

	include "template/footer.php"; ?>