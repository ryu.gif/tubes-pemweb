<?php include "template/header.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
            <h1>Daftar Ruang</h1>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">

          <div class="card-tools">
            <a href="tambah.php?id_lantai=<?php print $_GET['id_lantai'];?>"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</button></a>
          </div>
          <br>
        </div>
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Daftar Ruang</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th style="width:5%" class="text-center">No.</th>
                <th>Nama Ruangan</th>
                <th>Harga Ruangan</th>
                <th style="width:20%" class="text-center">Aksi</th>
              </tr>
              </thead>
              <tbody>
              <?php
                $id_lantai=$_GET['id_lantai'];
                $query="SELECT * FROM tabel_ruangan WHERE id_lantai='$id_lantai'";
                $query_exe=mysqli_query($conn, $query); $i=1;
                while($row=mysqli_fetch_array($query_exe)){
                ?>
                <tr>
                  <td class="text-center"><?php echo $i++; ?>.</td>
                  <td><?php echo $row['nama_ruangan']; ?></td>
                  <td><?php echo rupiah($row['harga_ruangan']); ?></td>
                  <td class="text-center">
                    <a href="editruang.php?id_ruangan=<?php echo $row['id_ruangan'].'&id_lantai='.$_GET['id_lantai']; ?>" type="button" class="btn btn-info btn-sm">Edit</a>
                    <a href="delete.php?id_ruangan=<?php echo $row['id_ruangan'].'&id_lantai='.$_GET['id_lantai']; ?>" type="button" class="btn btn-danger btn-sm">Delete</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>                
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $extra_js = "ruang_js.php"; include "template/footer.php"; ?>