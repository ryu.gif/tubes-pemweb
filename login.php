<?php include "private/function.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Login</title>
	<link rel="stylesheet" href="<?= base_url('assets_login/'); ?>bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url('assets_login/'); ?>css/login.css">

</head>
<body>
	<div class="container-fluid">
		<div class="card card-login">
			<div class="card-body">
				<div class="row justify-content-center">
					<div class="col-lg-6 col-md-12">
						<div class="padding bg-primary text-center align-items-center d-flex">
							<div id="particles-js"></div>
							<div class="w-100">
								<h2 class="text-light mb-4">Pagoda</h2>
                                <p class="text-light mb-4">- Enjoy Your Life - </p>
							</div>

							<!-- <div class="help-links">
								<ul>
									<li><a href="#">Terms of Service</a></li>
									<li><a href="#">Privacy Policy</a></li>
								</ul>
							</div> -->
						</div>
					</div>
					<div class="col-lg-6 col-md-12">
						<div class="padding" id="form_load">
                            <!-- Alert -->
                            <div id="this_alert">
                                <!-- alert here -->
                            </div>
                            <!-- End Alert -->
							<h2>Login</h2>
							<p class="lead">Silahkan masuk menggunakan akun anda. </p>
							<form autocomplete="off" id="login_form">
								<div class="form-group">
									<label for="username">Username</label>
									<input type="text" name="username" class="form-control" id="username" tabindex="1">
								</div>
								<div class="form-group">
									<label class="d-block" for="password">
										Password
										<!-- <div class="float-right">
											<a href="#">Lupa Password?</a>
										</div> -->
									</label>
									<input type="password" name="password" class="form-control" id="password" tabindex="2">
								</div>
								<div class="form-group text-right">
									<div class="float-left mt-2">
                                        Belum Punya Akun?<a href="<?= base_url('register.php'); ?>"> Daftar</a>
									</div>
									<input type="submit" class="btn btn-primary" tabindex="3" id="btn_login" value="Login" name="submit">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <script src="<?= base_url('assets/js/core/jquery.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
    <script>
		function disable_form() {
			$('[name="username"]').attr("disabled", true);
			$('[name="password"]').attr("disabled", true);
			$('#btn_login').attr("disabled", true);
		}

		function enable_form() {
			$('[name="username"]').attr("disabled", false);
			$('[name="password"]').attr("disabled", false);
			$('#btn_login').attr("disabled", false);
		}

		$('[name="submit"]').click(function(){
		});

        $('#login_form').submit(function(e){
            e.preventDefault();
			$.LoadingOverlay("show");
            $.ajax({
                url:'<?= base_url('process/auth.php'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(response){
                    var res = jQuery.parseJSON(response);
                    $("div#this_alert").html(res.pesan);
					if(res.status == "logged in"){
						disable_form();
						(function(){
						var counter = 3;
						setInterval(function() {
							counter--;
							if (counter >= 0) {
							span = document.getElementById("count_down");
							span.innerHTML = counter+' detik...';
							}
							// Display 'counter' wherever you want to display it.
							if (counter === 0) {
							//    alert('this is where it happens');
								clearInterval(counter);
								window.location.replace('<?= base_url(); ?>'+res.role+'/beranda.php');
							}

						}, 1000);
						})();
					}
                },
				error: function(){
					$.LoadingOverlay("hide", true);
				}
            });
			$.LoadingOverlay("hide", true);
            return false;
        });
    </script>
</body>
</html>