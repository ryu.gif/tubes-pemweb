<?php include "template/header.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Daftar User</h1>
          </div>
          <div class="col-sm-6">
            
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <section class="content">
      
      <!-- Default box -->
      <div class="box">
      <div class="box-header with-border">
          <h3 class="box-title">Daftar User</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>

        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>No.</th>
              <th>Nama Lengkap</th>
              <th>Role</th>
              <th>Status</th>
              <th style="width:5%">Aksi</th>
          </tr>
          </thead>
          <tbody>
          <?php
              $query = "SELECT * FROM tabel_user";
              $query_exe=mysqli_query($conn, $query); $i=1;
              while($row=mysqli_fetch_array($query_exe)){
          ?>
          <tr>
            <td><?php print $i++;?></td>
            <td><?php print $row['nama_lengkap'];?></td>
            <td><?php print $row['role'];?></td>
            <td><?php print $row['status'];?></td>
            <td>
				    <?php if($row['status']=='active'){ ?>
              <a href="modul/ubah_user.php?status=banned&id_user=<?=$row['id_user'];?>" class="btn btn-danger" role="button" title="Nonaktifkan Pengguna"><i class="fa fa-user-times"></i></a>
				    <?php } else { ?>
              <a href="modul/ubah_user.php?status=active&id_user=<?=$row['id_user'];?>" class="btn btn-success" role="button" title="Aktifkan Pengguna"><i class="fa fa-user"></i></a>
            <?php } ?>
            </td>
          </tr>
          <?php } ?>
          </tbody>
          </table>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $extra_js = "js.php";include "template/footer.php"; ?>