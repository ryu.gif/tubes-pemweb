<?php 

function base_url($url = null){
    // $currentPath = $_SERVER['PHP_SELF']; 
    // $pathInfo = pathinfo($currentPath); 
    // $hostName = $_SERVER['HTTP_HOST']; 
    // $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
    // return $protocol.$hostName.$pathInfo['dirname']."/".$url;
    return "http://localhost/hotel/".$url;
}

function redirect($url = null){
    header('Location: '.base_url().$url);
}

function tgl_indonesia($date){
    $date = date('Y-m-d',strtotime($date));
    if($date == '0000-00-00')
        return 'Tanggal Kosong';
 
    $tgl = substr($date, 8, 2);
    $bln = substr($date, 5, 2);
    $thn = substr($date, 0, 4);
 
    switch ($bln) {
        case 1 : {
                $bln = 'Januari';
            }break;
        case 2 : {
                $bln = 'Februari';
            }break;
        case 3 : {
                $bln = 'Maret';
            }break;
        case 4 : {
                $bln = 'April';
            }break;
        case 5 : {
                $bln = 'Mei';
            }break;
        case 6 : {
                $bln = "Juni";
            }break;
        case 7 : {
                $bln = 'Juli';
            }break;
        case 8 : {
                $bln = 'Agustus';
            }break;
        case 9 : {
                $bln = 'September';
            }break;
        case 10 : {
                $bln = 'Oktober';
            }break;
        case 11 : {
                $bln = 'November';
            }break;
        case 12 : {
                $bln = 'Desember';
            }break;
        default: {
                $bln = 'UnKnown';
            }break;
    } 
    $tanggalIndonesia = $tgl . " " . $bln . " " . $thn;
    return $tanggalIndonesia;
}

function encode($input) {
    return strtr(base64_encode($input), '+/=', '._-');
}

function decode($input){
    return base64_decode(strtr($input, '._-', '+/='));
}

function order_id()
{
    return substr(hash('sha256', mt_rand() . microtime()), 0, 5);
}

function rupiah($angka){
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
}

function selisih_hari($tgl1, $tgl2)
{
    if($tgl1 == $tgl2){
        $hasil = 1;
    }else{
        $start_date = new DateTime($tgl1);
        $end_date = new DateTime($tgl2);
        $interval = $start_date->diff($end_date);
        $hasil = $interval->days;
    }
    return $hasil;
}

?>