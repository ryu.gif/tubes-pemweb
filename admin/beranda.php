<?php include "template/header.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Hotel</h1>
          </div>
          <div class="col-sm-6"> 
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <a href="modul/tambah_hotel.php" class="btn btn-primary" role="button" title="Tambah Data"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
      <br>
      <br>
      <!-- Default box -->
      <div class="box">
      <div class="box-header with-border">
          <h3 class="box-title">Daftar Hotel</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- <div class="card-header">
          <h3 class="card-title"></h3>
          <div class="card-tools">
			
          </div>
        </div> -->
		
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>No.</th>
              <th>Nama Hotel</th>
              <th>Lokasi Hotel</th>
              <th>Bintang Hotel</th>
              <th>Fasilitas Hotel</th>
              <th>Catatan Hotel</th>
              <th>Aksi</th>
          </tr>
          </thead>
          <tbody>
          <?php
              $query = "SELECT * FROM tabel_hotel";
              $query_exe=mysqli_query($conn, $query); $i=1;
              while($row=mysqli_fetch_array($query_exe)){
          ?>
          <tr>
            <td><?php print $i++;?></td>
            <td><?php print $row['nama_hotel'];?></td>
            <td><?php print $row['lokasi_hotel'];?></td>
            <td><?php print $row['bintang_hotel'];?></td>
            <td><?php print $row['fasilitas_hotel'];?></td>
            <td><?php print $row['catatan_hotel'];?></td>
            <td>
              <a href="modul/ubah_hotel.php?id_hotel=<?=$row['id_hotel'];?>" class="btn btn-success" role="button" title="Ubah Data"><i class="glyphicon glyphicon-edit"></i></a>
              <a href="modul/hapus_hotel.php?id_hotel=<?=$row['id_hotel'];?>" class="btn btn-danger" role="button" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>
            </td>
          </tr>
          <?php } ?>
          </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $extra_js = "js.php";include "template/footer.php"; ?>