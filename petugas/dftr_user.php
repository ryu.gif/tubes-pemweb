<?php include "template/header.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Daftar User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Projects</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
          <div class="card-tools">
		  </div>
        </div>
		
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 10%">
                          Nomor
                      </th>
                      <th style="width: 20%">
                          Nama 
                      </th>
                      <th style="width: 30%">
                          Role
                      </th>
					  <th style="width: 30%">
                          Status
                      </th>
                      <th style="width: 20%">
						  Aksi
                      </th>
                  </tr>
              </thead>
              <tbody>
                <?php
                $query="SELECT * FROM tabel_user";
				$query_exe=mysqli_query($conn, $query); $i=1;
				while($row=mysqli_fetch_array($query_exe))
                {
                ?>

                <tr>
                  <td><?php print $i++;?></td>
                  <td><?php print $row['nama_lengkap'];?></td>
                  <td><?php print $row['role'];?></td>
				  <td><?php print $row['status'];?></td>
                  <td>
				  <?php if($row['status']=='active'){ ?>
                    <a href="modul/ubah_user.php?status=banned&id_user=<?=$row['id_user'];?>" class="btn btn-danger" role="button" title="Nonaktifkan Pengguna"><i class="fa fa-user-times"></i></a>
				  <?php } else { ?>
					<a href="modul/ubah_user.php?status=active&id_user=<?=$row['id_user'];?>" class="btn btn-success" role="button" title="Aktifkan Pengguna"><i class="fa fa-user"></i></a>
				  <?php } ?>
				  </td>
                </tr>

                <?php } ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include "template/footer.php"; ?>