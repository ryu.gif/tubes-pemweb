<?php include "template/header.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Lantai</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Project Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <?php
    if (isset($_GET['id_lantai'])){
      $datkode=$_GET['id_lantai'];
      $query="SELECT * FROM tabel_lantai WHERE id_lantai='$datkode'";
      $query_exe=mysqli_query($conn, $query);
      $row=mysqli_fetch_array($query_exe);	 								
    }
    ?>
    <section class="content">
    <div class="row">
      <div class="col-md-6">
      <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Ubah Lantai</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form action='' method='post'>
            <div class="box-body">
              <div class="form-group">
                <label for="nama_lantai">Nama Lantai</label>
                <input value="<?php print $row['nama_lantai'];?>" type="text" class="form-control" id="nama_lantai" name="nama_lantai" placeholder="Masukkan Nama Lantai" required>
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <input type="submit" class="btn btn-success pull-right" name="submit" value="Simpan">
            </div>
          </form>
        </div>
      </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <?php
  if(isset($_POST['submit'])){
		$nama_lantai=$_POST['nama_lantai'];
		$id_lantai=$_GET['id_lantai'];
		mysqli_query($conn, "UPDATE tabel_lantai SET nama_lantai='$nama_lantai' WHERE id_lantai='$id_lantai'");
    echo '<script>window.location.replace("'.base_url('petugas/beranda.php?id_lantai='.$_GET['id_lantai']).'")</script>';			 								
	}
  
  include "template/footer.php"; ?>