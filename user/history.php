<?php include "template/header.php"; ?>


  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Riwayat Pesanan
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Riwayat</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Daftar Pesanan Hotel</h3>
          </div>
          <div class="box-body">
            <ul class="timeline timeline-inverse">
                <!-- script here -->
                <?php $tgl_sebelumnya="";foreach ($process->list_riwayat($_SESSION['id_user']) as $row) { ?>
                <?php if($row['tgl_pesan'] != $tgl_sebelumnya){ ?>
                <!-- timeline time label -->
                <li class="time-label">
                    <span class="bg-red">
                        <?php echo $row['tgl_pesan']; ?>
                    </span>
                </li>
                <?php } ?>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                <i class="fa fa-ticket bg-blue"></i>

                <div class="timeline-item">
                    <h3 class="timeline-header"><a href="#">Anda</a> memesan tiket dengan kode booking <a href="<?= base_url('user/invoice.php?code='.$row['id_booking']); ?>"><b>#<?= $row['id_booking']; ?></b></a></h3>

                    <div class="timeline-body">
                    Anda Memesan tiket di hotel <b><?= $row['nama_hotel']; ?></b> atas nama <b><?= $row['nama_pemesan']; ?></b> dengan lama menginap yakni <?= $row['lama_menginap']; ?> hari untuk tanggal 
                    <?php if($row['start_booking'] == $row['end_booking']){
                      echo tgl_indonesia($row['start_booking']);
                    }else{ 
                      echo tgl_indonesia($row['start_booking'])." sampai ".tgl_indonesia($row['end_booking']);
                    } ?> 
                    dengan jumlah tamu yakni <?= $row['jumlah_tamu']; ?> orang dan kamar sejumlah <?= $row['jumlah_kamar'] ?> kamar. <br><br>
                    </div>
                    <div class="timeline-footer">
                    <a href="<?= base_url('user/invoice.php?code='.$row['id_booking']); ?>" class="btn btn-primary btn-xs">Lihat invoice</a>
                    </div>
                </div>
                </li>
                <?php $tgl_sebelumnya = $row['tgl_pesan']; } ?>
                <!-- END timeline item -->
                <li>
                <i class="fa fa-clock-o bg-gray"></i>
                </li>
            </ul>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  


<?php include "template/footer.php"; ?>