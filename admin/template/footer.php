<footer class="main-footer">
  <div class="container">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.2.1
    </div>
    <strong>Copyright &copy; <?= date('Y') ?> <a href="#">Pagoda</a>.</strong> All rights
    reserved.
  </div>
  <!-- /.container -->
</footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?= base_url('assets/')?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url('assets/')?>bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url('assets/')?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url('assets/')?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/')?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/')?>dist/js/demo.js"></script>
<script src="<?= base_url('assets/')?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/')?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<?php if(isset($extra_js)){include $extra_js; } ?>
</body>
</html>
