<?php include "template/header.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Ruangan</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

  <section class="content">
  <div class="row">
    <div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Ruangan</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form action='' method='post'>
          <div class="box-body">
            <div class="form-group">
              <label for="nama_ruangan">Nama Ruangan</label>
              <input type="text" class="form-control" id="nama_ruangan" name="nama_ruangan" placeholder="Masukkan Nama Ruangan" required>
            </div>
            <div class="form-group">
              <label for="harga_ruangan">Harga Ruangan</label>
              <input type="text" class="form-control" id="harga_ruangan" name="harga_ruangan" placeholder="Masukkan Harga Ruangan" required>
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <input type="submit" class="btn btn-success pull-right" name="submit" value="Simpan">
          </div>
        </form>
      </div>
    </div>
  </div>
  </section>


  <!-- /.content-wrapper -->
  </div>
  </div>

  <?php
  if(isset($_POST['submit'])){
		$nama_ruangan=$_POST['nama_ruangan'];
		$harga_ruangan=$_POST['harga_ruangan'];
		$id_lantai=$_GET['id_lantai'];
		mysqli_query($conn, "INSERT INTO tabel_ruangan (`nama_ruangan`, `harga_ruangan`, `id_lantai`)
										 VALUES
										('$nama_ruangan', '$harga_ruangan', '$id_lantai')");
		echo '<script>window.location.replace("'.base_url('petugas/ruang.php?id_lantai=').$_GET['id_lantai'].'")</script>';
		}

	include "template/footer.php"; ?>