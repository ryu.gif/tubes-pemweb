<?php include "template/header.php"; ?>

<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
<style>
.content-wrapper{
    background-color:#77F4FD;
}
.invoice-detail{
    background-color:#ffffff;
    padding: 15px 25px;
    border-radius:10px;
    box-shadow: 0 14px 28px rgba(0, 0, 0, 0.1), 0 10px 10px rgba(0, 0, 0, 0.13);
    font-family: "Roboto Condensed", sans-serif;
}

.judul{
    color: #344760 !important;
    font-weight: bold !important;
    font-size: 18px;
    padding-top:20px;
}

.sub-judul{
    color: #b7bcc3;
    font-size: 16px;
}

.sub-total{
  color: #b7bcc3;
  font-size: 12px;
  font-weight: normal;
}

.np{
    padding: 0px  !important;
}

table tr{
  font-size:16px;
  line-height: 2
}

.thanks{
  color: #b7bcc3;
  font-weight: bold;
}
/* 
table tr td{
  color: #b7bcc3;
} */

</style>

  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header text-center">
        <h1>
          &nbsp;
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 invoice-detail">
                <div class="row" style="padding :10px 20px">
                  <?php foreach($process->detail_booking($_GET['code']) as $detail){ ?>
                    <div class="col-md-6 np" style="line-height: 0.7;">
                        <p class="judul">INVOICE</p>
                        <p class="sub-judul">#<?= $_GET['code']; ?></p>
                    </div>
                    <div class="col-md-6 np" style="line-height: 0.7;">
                        <p style="color: #b7bcc3;font-size: 16px;float:right;padding-top:20px;"> Tanggal Pembelian : <?= $detail['tgl_pesan']; ?></p>
                        <p style="color: #b7bcc3;font-size: 16px;float:right"> Oleh : <?= $detail['nama_pemesan']; ?></p>
                    </div>
                    <div class="col-md-12" style="padding: 10px 50px;border-bottom: 2px dotted #b7bcc3"></div>
                    <div class="col-md-12 np" style="margin-bottom:30px;margin-top:20px;padding-bottom: 50px;">
                      <table style="width:100%">
                        <tr>
                          <th>Nomor Kamar</th>
                          <th class="pull-right">Harga</th>
                        </tr>
                        <?php foreach($process->list_room($_GET['code']) as $row){ ?>
                        <tr>
                          <td><?= 'Kamar '.$row['nama_ruangan']; ?></td>
                          <td class="pull-right"><?= rupiah($row['harga_ruangan']); ?></td>
                        </tr>
                        <?php } ?>
                        <tr style="border-top: 2px dotted #b7bcc3">
                          <th></th>
                          <th class="pull-right"><?= rupiah($detail['total']); ?></th>
                        </tr>
                        <tr>
                          <td>Lama Menginap</td>
                          <td class="pull-right"><?= $detail['lama_menginap']; ?> hari</td>
                        </tr>
                        <tr>
                          <th>Total Bayar</th>
                          <th class="pull-right"><?= rupiah(intval($detail['total'])*intval($detail['lama_menginap'])); ?></th>
                        </tr>
                        <tr>
                          <th></th>
                          <th class="pull-right sub-total">( <?= $detail['lama_menginap']; ?> hari * <?= rupiah($detail['total']); ?>)</th>
                        </tr>
                      </table>
                    </div>
                    <div class="col md-12 text-center thanks">
                      <h4>TERIMA KASIH TELAH MEMESAN DI PAGODA.</h4>
                      <p style="font-weight:normal">- Enjoy Your Life -</p>
                    </div>
                  <?php } ?>
                </div>
            </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  


<?php include "template/footer.php"; ?>