<?php include "template/header.php"; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesanan
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Pesanan</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>No.Booking</th>
                <th>Nama Pemesan</th>
                <th>Tgl Menginap</th>
                <th>Jumlah Kamar</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $id_hotel = $_SESSION['id_hotel'];
                $query = "SELECT DISTINCT a.id_booking, a.* FROM tabel_booking a
                LEFT JOIN tabel_room_choose b ON a.id_booking = b.id_booking
                LEFT JOIN tabel_ruangan c ON b.id_room = c.id_ruangan
                LEFT JOIN tabel_lantai d ON c.id_lantai = d.id_lantai
                LEFT JOIN tabel_hotel e ON d.id_hotel = e.id_hotel
                WHERE e.id_hotel='$id_hotel'
                AND a.aproved_by = '1'";
                $query_exe=mysqli_query($conn, $query); $i=1;
                while($row=mysqli_fetch_array($query_exe)){
            ?>
            <tr>
                <td>#<?= $row['id_booking']; ?></td>
                <td><?= $row['nama_pemesan']; ?></td>
                <td><?= tgl_indonesia($row['start_booking'])." - ".tgl_indonesia($row['end_booking'])." (".$row['lama_menginap']." hari)"; ?></td>
                <td><?= $row['jumlah_kamar']; ?> Kamar</td>
            </tr>
            <?php } ?>
            </tbody>
            </table>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $extra_js = "pesanan_js.php";include "template/footer.php"; ?>