<?php 
require "../private/function.php";
require "../connection.php";

    //reset session
    $_SESSION['is_login'] = FALSE;
    $_SESSION['nama'] = '';
    $_SESSION['username'] = '';
    $_SESSION['role'] = '';
    $_SESSION['id_hotel'] = '';

    $user = $_POST['username'];
    $pass = md5($_POST['password']);
    $status = "";
    $role = "";
    $sql = "SELECT * FROM tabel_user WHERE username = '$user' AND password = '$pass' LIMIT 1";
    $result = $conn->query($sql);

    if ($result->num_rows == 0) {
        $pesan = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Login gagal!</strong> Silahkan periksa username dan password anda.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
    }else{
        // session_start();
        while($row = $result->fetch_assoc()) {
            if($row['status'] == 'banned'){
                $pesan = "<div class='alert alert-warning alert-dismissible fade show' role='alert'><strong>Login gagal!</strong> Akun anda telah dinonaktifkan / dihapus<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                $status = "sleep";
            }else{
                $pesan = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Login berhasil!</strong> Anda akan dialihkan ke halaman pengguna dalam <span id='count_down'>3<span> detik...<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                $status = "logged in";
                $role = $row['role'];
                // session start
                session_start();
                $member_since = explode(" ",$row["created_"]);
                $_SESSION['is_login'] = TRUE;
                $_SESSION['id_user'] = $row["id_user"];
                $_SESSION['nama'] = $row["nama_lengkap"];
                $_SESSION['username']   = $row["username"];
                $_SESSION['since']   = $member_since[0];
                $_SESSION['role'] = $row["role"];
                if($row['role'] == 'petugas'){
                    $_SESSION['id_hotel'] = $row["id_hotel"];
                }
                $data = $row;
            }
        }
    }
    $response = array(
        'pesan' => $pesan,
        'status' => $status,
        'role' => $role,
    );
    echo json_encode($response);

?>