<?php include "private/function.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Create New Account</title>
	<link rel="stylesheet" href="<?= base_url('assets_login/'); ?>bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url('assets_login/'); ?>css/login.css">

</head>
<body>
	<div class="container-fluid">
		<div class="card card-login">
			<div class="card-body">
				<div class="row justify-content-center">
					<div class="col-lg-6 col-md-12">
						<div class="padding bg-primary text-center align-items-center d-flex">
							<div id="particles-js"></div>
							<div class="w-100">
								<h2 class="text-light mb-4">Create</h2>
                                <h2 class="text-light mb-4">New Account</h2>
							</div>

							<!-- <div class="help-links">
								<ul>
									<li><a href="#">Terms of Service</a></li>
									<li><a href="#">Privacy Policy</a></li>
								</ul>
							</div> -->
						</div>
					</div>
					<div class="col-lg-6 col-md-12">
						<div class="padding" id="form_load">
                            <!-- Alert -->
                            <div id="this_alert">
                                <!-- alert here -->
                            </div>
                            <!-- End Alert -->
							<h2>Register</h2>
							<p class="lead">Silahkan lengkapi data diri anda. </p>
							<form autocomplete="off" id="login_form" class="needs-validation" novalidate method="post" action="<?= base_url('process/register.php'); ?>">
                                <div class="form-group">
									<label for="nama_lengkap">Nama Lengkap</label>
									<input type="text" name="nama_lengkap" class="form-control" id="nama_lengkap" tabindex="0" required>
									<div class="invalid-feedback">
										Nama harus diisi.
									</div>
								</div>
                                <div class="form-group">
									<label for="username">Username</label>
									<input type="text" name="username" class="form-control" id="username" required>
									<div class="invalid-feedback username-invalid">
										Username harus disii.
									</div>
								</div>
								<div class="form-group">
									<label class="d-block" for="password">
										Password
									</label>
									<input type="password" name="password" class="form-control" id="password" required>
									<div class="invalid-feedback">
										Password harus diisi.
									</div>
								</div>
                                <div class="form-group">
									<label class="d-block" for="repassword">
										Ulangi Password
									</label>
									<input type="password" name="repassword" class="form-control" id="repassword" required>
									<div class="invalid-feedback">
										Password tidak sama.
									</div>
								</div>
								<div class="custom-control custom-checkbox mb-3">
									<input type="checkbox" class="custom-control-input" id="customCheck" name="hotel_owner">
									<label class="custom-control-label" for="customCheck">Saya adalah pemilik hotel.</label>
								</div>
								<div class="owner_form">
									<div class="form-group">
										<label for="nama_hotel">Nama Hotel</label>
										<input type="text" name="nama_hotel" class="form-control" id="nama_hotel">
										<div class="invalid-feedback hotel-invalid">
											Nama hotel harus disii.
										</div>
									</div>
									<div class="form-group">
										<label for="lokasi_hotel">Lokasi</label>
										<input type="text" name="lokasi_hotel" class="form-control" id="lokasi_hotel">
										<div class="invalid-feedback">
											Lokasi hotel harus disii.
										</div>
									</div>
								</div>
								<div class="form-group text-right">
									<div class="float-left mt-2">
                                        Sudah punya akun?<a href="<?= base_url('login.php'); ?>"> Login</a>
									</div>
									<input type="submit" class="btn btn-primary" tabindex="3" id="btn_register" value="Register" name="submit">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <script src="<?= base_url('assets/js/core/jquery.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
    <script>

        $(document).ready(function(){
			$(".owner_form").hide();
			$(function(){
				$('[name="hotel_owner"]').change(function()
				{
					if ($(this).is(':checked')) {
						$(".owner_form").show();
						$('#nama_hotel').attr("required", "required");
						$('#lokasi_hotel').attr("required", "required");
					}else{
						$(".owner_form").hide();
						$('#nama_hotel').removeAttr("required");
						$('#lokasi_hotel').removeAttr("required");
					}
				});
			});
			
            var password = document.getElementById("password")
			, confirm_password = document.getElementById("repassword");

			function validatePassword(){
				if(password.value != confirm_password.value) {
					confirm_password.setCustomValidity("Passwords tidak sama.");
				} else {
					confirm_password.setCustomValidity('');
				}
			}

			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;
        });

        (function() {
		'use strict';
		window.addEventListener('load', function() {
			// Fetch all the forms we want to apply custom Bootstrap validation styles to
			var forms = document.getElementsByClassName('needs-validation');
			// Loop over them and prevent submission
			var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if (form.checkValidity() === false) {
				event.preventDefault();
				event.stopPropagation();
				}
				form.classList.add('was-validated');
			}, false);
			});
		}, false);
		})();

		$( "#username" ).change(function() {
			if(this.value.length == 0 ){
				$('.username-invalid').html("Username harus diisi.");
				$( ".username-invalid" ).removeClass( "valid-feedback" ).addClass( "invalid-feedback" );
			}
		});

		$( "#username" ).keyup(function() {
			// var valid = username_valid(this.value);
			// if(valid == true){
				if(this.value.length >= 3){
					check_username(this.value);
				}else{
					$('.username-invalid').html("Username minimal 3 karakter.");
					$( ".username-invalid" ).removeClass( "valid-feedback" ).addClass( "invalid-feedback" );
				}
			// }
		});

		$( "#nama_hotel" ).change(function() {
			if(this.value.length == 0 ){
				$('.hotel-invalid').html("Nama hotel harus diisi.");
				$( ".hotel-invalid" ).removeClass( "valid-feedback" ).addClass( "invalid-feedback" );
			}
		});

		$( "#nama_hotel" ).keyup(function() {
			if(this.value.length >= 3){
				check_hotel(this.value);
			}else{
				$('.hotel-invalid').html("Nama hotel minimal 3 karakter.");
				$( ".hotel-invalid" ).removeClass( "valid-feedback" ).addClass( "invalid-feedback" );
			}
		});

		function check_username(params) {
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('process/check_username.php')?>",
                dataType : "JSON",
                data : {username:params},
                success: function(response){
					console.log(response.status);
					$('.username-invalid').html(response.text).show();
					if(response.status == "not available"){
						$( ".username-invalid" ).removeClass( "valid-feedback" ).addClass( "invalid-feedback" );
					}else if(response.status == "available"){
						$( ".username-invalid" ).removeClass( "invalid-feedback" ).addClass( "valid-feedback" );
					}

                }
            });
            return false;
		}

		// function username_valid(params){
		// 	var expr = /^[a-zA-Z0-9_]{3,10}$/;
		// 	if (!expr.test(username)) {
		// 		$('.username-invalid').html("Username hanya boleh huruf, angka dan underscore.");
		// 		$( ".username-invalid" ).removeClass( "valid-feedback" ).addClass( "invalid-feedback" );
		// 		return true;
		// 	}else{
		// 		return false;
		// 	}
		// }

		function check_hotel(params){
			$.ajax({
                type : "POST",
                url  : "<?php echo base_url('process/check_hotel.php')?>",
                dataType : "JSON",
                data : {hotel:params},
                success: function(response){
					console.log(response.status);
					$('.hotel-invalid').html(response.text).show();
					if(response.status == "not available"){
						$( ".hotel-invalid" ).removeClass( "valid-feedback" ).addClass( "invalid-feedback" );
					}else if(response.status == "available"){
						$( ".hotel-invalid" ).removeClass( "invalid-feedback" ).addClass( "valid-feedback" );
					}

                }
            });
            return false;
		}


    </script>
</body>
</html>