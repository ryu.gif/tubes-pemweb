<?php  include "template/header.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
            <h1>Daftar Lantai</h1>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        </div>
          <a href="tambahl.php?id_hotel=<?php print $_SESSION['id_hotel'];?>"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</button></a>
          <br>
          <br>

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Lantai</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="width:5%" class="text-center">No.</th>
                  <th>Nama Lantai</th>
                  <th style="width:20%" class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $id_hotel = $_SESSION['id_hotel'];
                  $query = "SELECT * FROM tabel_lantai WHERE id_hotel='$id_hotel'";
                  $query_exe=mysqli_query($conn, $query); $i=1;
                  while($row=mysqli_fetch_array($query_exe)){
                ?>
                  <tr>
                    <td class="text-center"><?php echo $i++; ?>.</td>
                    <td><?php echo $row['nama_lantai']; ?></td>
                    <td class="text-center">
                      <a href="ruang.php?id_lantai=<?php echo $row['id_lantai'];?>" type="button" class="btn btn-primary btn-sm">Daftar Ruang</a>
                      <a href="edit.php?id_lantai=<?php echo $row['id_lantai'];?>" type="button" class="btn btn-info btn-sm">Edit</a>
                      <a href="dellantai.php?id_lantai=<?php echo $row['id_lantai'];?>" type="button" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php  $extra_js = "beranda_js.php";include "template/footer.php"; ?>