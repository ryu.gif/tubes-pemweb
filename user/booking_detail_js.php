<script>
$(document).ready(function() {
  $("#jumlah_tamu, #jumlah_kamar").inputFilter(function(value) {
    return /^\d*$/.test(value);    // Allow digits only, using a RegExp
  });
});

(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
}(jQuery));

$('#tgl_menginap').daterangepicker({
    "autoApply": true,
    "startDate": new Date(),
    locale: {
        separator: ' sampai ',
        format: 'DD MMMM YYYY'
    }
}, function(start, end, label) {
  
});

$('#jumlah_kamar').on('change',function(){
   $('#pilih_kamar').select2({ maximumSelectionLength: this.value });
   $("#pilih_kamar").val("").trigger("change");
});

$("#pilih_kamar").select2({
    maximumSelectionLength: 1
});

</script>