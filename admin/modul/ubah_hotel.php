<?php include "../template/header.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ubah Data Hotel</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
		
        <div class="card-body p-0">
          <form method="post">
			<?php
				if (isset($_GET['id_hotel'])){
				$datkode=$_GET['id_hotel'];
				$query="SELECT * FROM tabel_hotel WHERE id_hotel='$datkode'";
				$query_exe=mysqli_query($conn, $query);
				$row=mysqli_fetch_array($query_exe);
				
				
				if(isset($_POST['submit'])){
				
				$nama_hotel=$_POST['nama_hotel'];
				$lokasi_hotel=$_POST['lokasi_hotel'];
				$bintang_hotel=$_POST['bintang_hotel'];
				$fasilitas_hotel=$_POST['fasilitas_hotel'];
				$catatan_hotel=$_POST['catatan_hotel'];
				
		
			$query="UPDATE tabel_hotel
				SET
				nama_hotel='$nama_hotel',
				lokasi_hotel='$lokasi_hotel',
				bintang_hotel='$bintang_hotel',
				fasilitas_hotel='$fasilitas_hotel',
				catatan_hotel='$catatan_hotel'
				WHERE
				id_hotel='$datkode'";
				$query_exe=mysqli_query($conn, $query);
				echo '<script>window.location.replace("'.base_url('admin/beranda.php').'")</script>';			 								
	}
		?>
		
		<div class="box box-primary">
		<div class="box-body">
		
		<div class="form-group">
		<label> Nama Hotel </label> 
		<input type="text" name="nama_hotel" value="<?php print $row['nama_hotel'];?>" 
		class="form-control" placeholder="Nama Hotel" required>
        </div><br> 
		
		<div class="form-group">
		<label> Lokasi Hotel </label> 
		<input type="text" name="lokasi_hotel" value="<?php print $row['lokasi_hotel'];?>"
		class="form-control" placeholder="Lokasi Hotel" required>
        </div><br> 
		
		<div class="form-group">
		<label> Bintang Hotel </label> 
		<select class="form-control" name="bintang_hotel" value="<?php print $row['bintang_hotel'];?>" >
                    <option value="">- Pilih Bintang Hotel -</option>
                    <option <?php if($row['bintang_hotel'] == '1'){echo "selected";} ?> value="1">1</option>
                    <option <?php if($row['bintang_hotel'] == '2'){echo "selected";} ?> value="2">2</option>
                    <option <?php if($row['bintang_hotel'] == '3'){echo "selected";} ?> value="3">3</option>
                    <option <?php if($row['bintang_hotel'] == '4'){echo "selected";} ?> value="4">4</option>
					<option <?php if($row['bintang_hotel'] == '5'){echo "selected";} ?> value="5">5</option>
        </select>
        </div><br> 
		
		<div class="form-group">
		<label> Fasilitas Hotel </label> 
		<input type="text" name="fasilitas_hotel" value="<?php print $row['fasilitas_hotel'];?>"  
		class="form-control" placeholder="Fasilitas Hotel" required>
        </div><br> 
		
		<div class="form-group">
		<label> Catatan Hotel </label> 
		<input type="text" name="catatan_hotel" value="<?php print $row['catatan_hotel'];?>" 
		class="form-control" placeholder="Catatan Hotel" required>
        </div><br> 
	
		<input name="submit" type="submit" class="btn btn-primary pull-right" title="Ubah Data" value="Ubah Data Hotel">
		
		</div>
		</div>
	
		<?php } ?>
		</form>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include "../template/footer.php"; ?>