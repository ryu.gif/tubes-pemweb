<?php 
require "../private/function.php";
require "../connection.php";

$nama_hotel = $_POST['hotel'];
$sql = "SELECT * FROM tabel_hotel WHERE nama_hotel = '$nama_hotel'";
$result = $conn->query($sql);

if ($result->num_rows == 0) {
    $status = "available";
    $text = "Nama hotel tersedia.";
}else{
    $status = "not available";
    $text = "Nama hotel sudah dipakai.";
}

$response = [
    'status' => $status,
    'text' => $text
];

echo json_encode($response);

?>