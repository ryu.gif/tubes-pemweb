<?php include "template/header.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Lantai</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Project Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

   
    <!-- Main content -->
	<form action='' method='post'>
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Lantai</h3>
              
            </div>
            <div class="card-body">
              <div class="form-group">
                <input type="text" id="inputName" class="form-control">
              </div>
              
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      <div class="row">
        <div class="col-12">
          <a href="beranda.php" class="btn btn-primary btn-sm">Cancel</a>
          <a href="ruang.php"><input type="submit" value="Simpan Perubahan" class="btn btn-success float-right">
        </div>
      </div>
	  </div>
    <!-- /.content -->
  </section>
  </form>
  <!-- /.content-wrapper -->
  </div>
  </div>
  
  <?php include "template/footer.php"; ?>