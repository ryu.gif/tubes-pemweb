<?php include "template/header.php"; ?>

<style>
.img-hotel{
  margin-bottom:30px;
  /* filter:brightness(70%); */
  position: relative;
  text-align: center;
  color: black;
}

.img-hotel:hover{
  /* filter:brightness(100%); */
}

/* Bottom left text */
.bottom-left {
  position: absolute;
  bottom: 4%;
  left: 8%;
  background-color:white;
  padding:5px 10px;
  border-radius:5px;
}

.top-right {
  position: absolute;
  top: 4%;
  right: 8%;
  background-color:#f39c12 ;
  padding:5px 10px;
  border-radius:5px;
  color:white;
}

.top-left {
  position: absolute;
  top: 4%;
  left: 8%;
  background-color:#f39c12 ;
  padding:5px 10px;
  border-radius:5px;
  color:white;
}

</style>

  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Pesan Hotel Anda Sekarang Juga!
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li class="active">Pesan Hotel</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <?php foreach($process->hotel_list() as $row){ ?>
            <a href="booking_detail.php?hotel=<?= encode($row['id_hotel']); ?>">
            <div class="col-sm-3 img-hotel">
              <img src="<?= base_url('uploads/hotel/hotel.jpg'); ?>" alt="<?= $row['nama_hotel']; ?>" style="width:100%;">
              <div class="top-left"><i class="fa fa-map-marker"></i> <?= $row['lokasi_hotel']; ?></div>
              <div class="top-right"><i class="fa fa-star"></i> <?= $row['bintang_hotel']; ?></div>
              <div class="bottom-left"><?= $row['nama_hotel']; ?></div>
            </div>
            </a>
          <?php } ?>
        </div>
        <!-- <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    <h3 class="box-title">Detail Pesanan</h3>
                    </div>
                    <form role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="nama_pemesan">Nama Pemesan</label>
                            <input type="text" class="form-control" id="nama_pemesan" placeholder="Masukkan Nama Pemesan" value="<?= $_SESSION['nama']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="nama_hotel">Hotel</label>
                            <input type="text" class="form-control" id="nama_hotel" placeholder="Masukkan Nama Hotel">
                        </div>
                        <div class="form-group">
                            <label for="tgl_menginap">Tanggal Menginap</label>
                            <input type="text" class="form-control" id="tgl_menginap" placeholder="Pilih Tanggal Menginap">
                        </div>
                        

                        
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Pesan Hotel</button>
                    </div>
                    </form>
                </div>
            </div>
        </div> -->
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  
<?php 
    // $extra_js = 'booking_js.php';
    include "template/footer.php"; 
?>